const assert = require('assert');
const app = require('../../src/app');

describe('\'bundles\' service', () => {
  it('registered the service', () => {
    const service = app.service('bundles');

    assert.ok(service, 'Registered the service');
  });
});
