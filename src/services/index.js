const users = require('./users/users.service.js');
const tokens = require('./tokens/tokens.service.js');
const bundles = require('./bundles/bundles.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(tokens);
  app.configure(bundles);
};
