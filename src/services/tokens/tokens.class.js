const { Service } = require('feathers-nedb');

exports.Tokens = class Tokens extends Service {
  create (data, params) {
   
    const { token, denom, used } = data;
    
    
    const tokenData = {
      token,
      denom,
      used
      
    };

    
    return super.create(tokenData, params);
  } 
};
