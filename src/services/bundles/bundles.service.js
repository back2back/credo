// Initializes the `bundles` service on path `/bundles`
const { Bundles } = require('./bundles.class');
const createModel = require('../../models/bundles.model');
const hooks = require('./bundles.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/bundles', new Bundles(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('bundles');

  service.hooks(hooks);
};
