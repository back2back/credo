const { Service } = require('feathers-nedb');

exports.Bundles = class Bundles extends Service {

	create (data, params) {
    // This is the information we want from the user signup data
    const { denom } = data;
    
    // Create bundle
    const bundleData = {
      denom 
    };

    // Call the original `create` method with existing `params` and new data
    return super.create(bundleData, params);
  } 
  
};
