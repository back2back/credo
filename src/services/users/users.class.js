// This is the database adapter service class
const { Service } = require('feathers-nedb');
// We need this to create the MD5 hash
const crypto = require('crypto');


// The size query. Our chat needs 60px images
const query = 's=60';


exports.Users = class Users extends Service {
  create (data, params) {
    // This is the information we want from the user signup data
    const { email, password, name } = data;
    
    // The complete user
    const userData = {
      email,
      name,
      password  
    };

    // Call the original `create` method with existing `params` and new data
    return super.create(userData, params);
  }  
};
