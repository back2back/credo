// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {

  	 const { data } = context;

     console.log(data);
    // Throw an error if we didn't get a text
    if(!data.token) {
      throw new Error('Please enter some number');
    }

    
    // Make sure that tokens are no longer than 16 characters
    const token = data.token.substring(0, 16);
    const denom = data.denom;
    const used = 0;

    if(!denom) {
      throw new Error('Please enter denomination');
    }
    context.data ={
      token,
      denom,
      used
    };

    return context;
  };
};
