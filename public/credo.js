// Establish a Socket.io connection
const socket = io();
// Initialize our Feathers client application through Socket.io
// with hooks and authentication.
const client = feathers();

client.configure(feathers.socketio(socket));
// Use localStorage to store our login token
client.configure(feathers.authentication());

// Login screen
const loginHTML = `<main class="login container">
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet text-center heading">
      <h1 class="font-100">Log in or signup</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
      <form class="form">
        <fieldset>
          <input class="block" type="email" name="email" placeholder="email">
        </fieldset>

        <fieldset>
          <input class="block" type="password" name="password" placeholder="password">
        </fieldset>

        <button type="button" id="login" class="button button-primary block signup">
          Log in
        </button>

        <button type="button" id="signup" class="button button-primary block signup">
          Sign up and log in
        </button>

        
      </form>
    </div>
    

  </div>
</main>`;


const choiceHTML = `<main class="flex flex-column">
  <header class="title-bar flex flex-row flex-center">
    <div class="title-wrapper block center-element">
      <img class="logo" src="http://feathersjs.com/img/feathers-logo-wide.png"
        alt="Feathers Logo">
      <span class="title">Credo</span>
    </div>
  </header>

  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
    	<button type="button" id="cashier" class="button button-primary block signup">
	          Cashier
	    </button>

	    <button type="button" id="admin" class="button button-primary block signup">
	          Admin
	    </button>
    </div>
  	
    
  </div>
</main>`;

// admin screen
const adminHTML =  `<main class="flex flex-column">
  <header class="title-bar flex flex-row flex-center">
    <div class="title-wrapper block center-element">
      <img class="logo" src="http://feathersjs.com/img/feathers-logo-wide.png"
        alt="Feathers Logo">
      <span class="title">Admin credo</span>
    </div>
  </header>

  <div class="flex flex-row flex-1 clear">
    <aside class="sidebar col col-3 flex flex-column flex-space-between">
      
      <form class="flex flex-row flex-space-between" id="bundleCreate">
        <input type="number" name="denom" class="flex flex-1">
        <button class="button-primary" type="submit">Create bundle</button>
      </form>
      
      <header class="flex flex-row flex-center">
        <h4 class="font-300 text-center">
          Bundles list
        </h4>
      </header>

      <ul id = "list-listener" class="flex flex-column flex-1 list-unstyled bundle-list"></ul>
      <footer class="flex flex-row flex-center">
        <a href="#" id="logout" class="button button-primary">
          Sign Out
        </a>
      </footer>
    </aside>

    <div class="flex flex-column col col-9">
      <main class="credo flex flex-column flex-1 clear">
        
      </main>

      <form class="flex flex-row flex-space-between" id="send-message">
        
        <input type="text" name="text" class="flex flex-1" >
        
        <button class="button-primary" type="submit">Send</button>
      </form>
    </div>
  </div>
</main>`;

const cashierHTML = `<main class="flex flex-column">
  <header class="title-bar flex flex-row flex-center">
    <div class="title-wrapper block center-element">
      <img class="logo" src="http://feathersjs.com/img/feathers-logo-wide.png"
        alt="Feathers Logo">
      <span class="title">Cashier credo</span>
    </div>
  </header>

  <div class="flex flex-row flex-1 clear">
    <aside class="sidebar col col-3 flex flex-column flex-space-between">
   
      <header class="flex flex-row flex-center">
        <h4 class="font-300 text-center">
          Bundles list
        </h4>
      </header>

      <ul id = "list-listener" class="flex flex-column flex-1 list-unstyled bundle-list"></ul>
      <footer class="flex flex-row flex-center">
        <a href="#" id="logout" class="button button-primary">
          Sign Out
        </a>
      </footer>
    </aside>

    
    <div class="flex flex-column col col-9">
      <p> Click on token to copy to clipboard</p>
      <main class="credo flex flex-column flex-1 clear">
         
      </main>

      
    </div>
  </div>
</main>`;

const listBundles = bundle => {
  const bundleList = document.querySelector('.bundle-list');

  if(bundleList) {
    // Add the bundle to the list
    bundleList.innerHTML += `<li>
      <a class="block relative" href="#" onclick = "wt( ${(bundle.denom )} ); return false;">
       
        <span class="absolute username">${(bundle.denom )}</span>
      </a>
    </li>`;

    
  }
};





const wt = bundle => {
const tt = client.service('bundles').find({
  query: {
    denom: `${(bundle)}`
  }
})
.then(tt => populateList(tt));;
  
denomSelected(bundle);
};

const populateList = list => {
  
  showChat(list);
  tokenDenom(list);
};


 
const denomSelected = bundle => {
  const whichDenom = document.querySelector('.credo');

  if (whichDenom) {
    whichDenom.innerHTML = `<h3 id = "bundleValue" > Bundle value:
      ${(bundle )}
    </h3>`
  }

  //const formvalue = document.getElementById('send-message');
  //formvalue.innerHTML += `<input type="hidden" name="look" value= ${(bundle)} />`

  if (document.contains(document.getElementById("look"))) {
      document.getElementById("look").value = bundle;
  }   else {
    
    if (document.contains(document.getElementById("bundleCreate"))) {
             //lastDiv.appendChild(submitButton);  
          const formvalue = document.getElementById('send-message');
    formvalue.innerHTML += `<input type="hidden" id="look" value= ${(bundle)} />`
  } 
          
  }
};

// Renders a message to the page
const addMessage = message => {
  
  const chat = document.querySelector('.credo');
  
  const token = (message.token);
  
  if(chat) {
    chat.innerHTML += `<div class="message-wrapper">
        
        <p class="message-content font-300">${token}</p>

      </div>
    </div>`;

    // Always scroll to the bottom of our message list
    chat.scrollTop = chat.scrollHeight - chat.clientHeight;
  }
};

const copied = message => {
  
  const wrapper = document.querySelector('.message-wrapper');
    wrapper.remove();
}

// Show the login page
const showLogin = (error) => {
  if(document.querySelectorAll('.login').length && error) {
    document.querySelector('.heading').insertAdjacentHTML('beforeend', `<p>There was an error: ${error.message}</p>`);
  } else {
    document.getElementById('app').innerHTML = loginHTML;
  }
};

// Shows the chat page
const showChat = async (f) => {
  //document.getElementById('app').innerHTML = chatHTML;
  const foobar = f.data[0].denom;
  
  
  // Find the latest 25 messages. They will come with the newest first
  const tokens = await client.service('tokens').find({
    query: {
      denom: `${(foobar)}`,
      used: 0,
      $limit: 25
    }
  });

 
  
  // We want to show the newest message last
  tokens.data.reverse().forEach(addMessage);

  
};

const tokenDenom = async (y) => {
  const uji = y.data[0].denom;
};

// Shows the chat page
const showChoice = async () => {
  document.getElementById('app').innerHTML = choiceHTML;  
  //depending on choice add event listener
};

// Shows the chat page
const showAdmin = async () => {

  const bundles = await client.service('bundles').find(); 
  bundles.data.forEach(listBundles);
  
};

const showCashier = async () => {
  
  const bundles = await client.service('bundles').find(); 
  bundles.data.forEach(listBundles);  
};


// Retrieve email/password object from the login/signup page
const getCredentials = () => {
  const user = {
    email: document.querySelector('[name="email"]').value,
    password: document.querySelector('[name="password"]').value
  };

  return user;
};

const getToken = async (tokenData) => {
  const token = await client.service('tokens').get(tokenData);

  //update
  var foo = {
    used: 1
  }
  client.service('tokens').patch(tokenData, foo);
}

// Log in either using the given email/password or the token from storage
const login = async credentials => {
  try {
    if(!credentials) {
      // Try to authenticate using an existing token
      await client.reAuthenticate();
    } else {
      // Otherwise log in with the `local` strategy using the credentials we got
      await client.authenticate({
        strategy: 'local',
        ...credentials
      });
    }

    // If successful, show the chat page
    showChoice();
  } catch(error) {
    // If we got an error, show the login page
    showLogin(error);
  }
};

const addEventListener = (selector, event, handler) => {
  document.addEventListener(event, async ev => {
    if (ev.target.closest(selector)) {
      handler(ev);
    }
  });
};

// "Signup and login" button click handler
addEventListener('#signup', 'click', async () => {
  // For signup, create a new user and then log them in
  const credentials = getCredentials();
    
  // First create the user
  await client.service('users').create(credentials);
  // If successful log them in
  await login(credentials);
});

// "Login" button click handler
addEventListener('#login', 'click', async () => {
  const user = getCredentials();

  await login(user);
});

// "Logout" button click handler
addEventListener('#logout', 'click', async () => {
  await client.logout();
    
  document.getElementById('app').innerHTML = loginHTML;
});

// "admin" button click handler
addEventListener('#admin', 'click', async () => {   
  document.getElementById('app').innerHTML = adminHTML;
  showAdmin();
});

addEventListener('#cashier', 'click', async () => {
  // For signup, create a new user and then log them in
  document.getElementById('app').innerHTML = cashierHTML;
  showCashier();
});

//create bundle
addEventListener('#bundleCreate', 'submit', async ev => {
  // This is the message text input field
  const input = document.querySelector('[name="denom"]');

  ev.preventDefault();

  // Create a new message and then clear the input field
  await client.service('bundles').create({
    denom: input.value
  });

  input.value = '';
});

// "Send" message form submission handler
addEventListener('#send-message', 'submit', async ev => {
  // This is the message text input field
  const input = document.querySelector('[name="text"]');

  const vdenom = document.querySelector('[id="look"]');
  
  ev.preventDefault();

  // Create a new message and then clear the input field
  await client.service('tokens').create({
    token: input.value,
    denom: vdenom.value
  });

  input.value = '';
});

// "Logout" button click handler
addEventListener('#logout', 'click', async () => {  
  document.getElementById('app').innerHTML = loginHTML;
});

addEventListener('.message-content', 'click', async (e) => {  
  e = e || window.event;
    var target = e.target || e.srcElement,
        text = target.textContent || target.innerText;
  
  //if bundle creation admin view so dont listen
  if (document.contains(document.getElementById("bundleCreate"))) {
    console.log('admin');
  } else{
    
    const el = document.createElement('textarea');
    el.value = `*130*${(text)}#`;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    //patch as used and remove from list. 
    getToken(text);
  }
  
});

// Listen to created events and add the new message in real-time
client.service('bundles').on('created', listBundles);
client.service('tokens').on('created', addMessage);
client.service('tokens').on('patched', copied);
// Call login right away so we can show the chat window
// If the user can already be authenticated
login();

